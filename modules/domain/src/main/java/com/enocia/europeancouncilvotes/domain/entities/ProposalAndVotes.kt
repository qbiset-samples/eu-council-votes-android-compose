package com.enocia.europeancouncilvotes.domain.entities

interface ProposalAndVotes {
    val proposal: Proposal
    val votes: List<VoteAndCountry>
}