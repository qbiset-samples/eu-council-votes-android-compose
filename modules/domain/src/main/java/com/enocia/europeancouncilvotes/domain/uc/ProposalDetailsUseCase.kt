package com.enocia.europeancouncilvotes.domain.uc

import com.enocia.europeancouncilvotes.domain.entities.ProposalAndVotes
import com.enocia.europeancouncilvotes.domain.repositories.ProposalsRepository

class ProposalDetailsUseCase(private val proposalsRepository: ProposalsRepository) {
    companion object {
        private const val TAG = "ProposalDetailsUseCase"
    }

    fun getProposalDetails(proposalId: Int): ProposalAndVotes = proposalsRepository.getProposalAndVotes(proposalId)
}