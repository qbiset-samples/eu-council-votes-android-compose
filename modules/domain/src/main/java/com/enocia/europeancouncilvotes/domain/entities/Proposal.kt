package com.enocia.europeancouncilvotes.domain.entities

import java.time.LocalDate

/**
 * A proposal the European Council voted on.
 */
interface Proposal {
    val identifier: Int
    val title: String
    val pdfLink: String
    val date: LocalDate
}