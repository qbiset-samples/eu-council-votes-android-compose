package com.enocia.europeancouncilvotes.domain.entities

/**
 * Represents the vote of a given country on a given proposal.
 */
interface Vote {
    val proposalId: Int
    val countryId: Int
    val value: Value

    enum class Value { FOR, AGAINST, ABSTAIN, NOT_PARTICIPATING }
}