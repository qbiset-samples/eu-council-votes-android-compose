package com.enocia.europeancouncilvotes.domain.entities

interface VoteAndCountry {
    val country: Country
    val value: Vote.Value
}