package com.enocia.europeancouncilvotes.domain.repositories

interface CountriesRepository {
    /**
     * Fetches countries from a remote data source to store them locally.
     */
    suspend fun loadCountries(): Result<Unit>
}