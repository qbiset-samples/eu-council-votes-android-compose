package com.enocia.europeancouncilvotes.domain.repositories

import com.enocia.europeancouncilvotes.domain.entities.Proposal
import com.enocia.europeancouncilvotes.domain.entities.ProposalAndVotes
import kotlinx.coroutines.flow.Flow

interface ProposalsRepository {
    /**
     * Fetches countries from a remote data source to store them locally.
     */
    suspend fun loadProposalsAndVotes(): Result<Unit>
    /**
     * Retrieves a flow of Proposal items from storage.
     */
    fun getProposalsAsFlow(): Flow<List<Proposal>>
    /**
     * Retrieves all data concerning a proposal from storage.
     */
    fun getProposalAndVotes(proposalId: Int): ProposalAndVotes
}