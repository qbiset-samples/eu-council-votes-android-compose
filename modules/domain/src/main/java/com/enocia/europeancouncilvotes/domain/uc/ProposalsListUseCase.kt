package com.enocia.europeancouncilvotes.domain.uc

import com.enocia.europeancouncilvotes.domain.entities.Proposal
import com.enocia.europeancouncilvotes.domain.repositories.CountriesRepository
import com.enocia.europeancouncilvotes.domain.repositories.ProposalsRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.withContext

class ProposalsListUseCase(
    private val countriesRepository: CountriesRepository,
    private val proposalsRepository: ProposalsRepository
) {
    companion object {
        private const val TAG = "ProposalsListUseCase"
    }

    /**
     * Retrieves a flow of Proposal items from storage.
     */
    fun getProposalsAsFlow(): Flow<List<Proposal>> = proposalsRepository.getProposalsAsFlow()

    /**
     * Queries the remote data sources and stores the retrieved data locally.
     */
    suspend fun loadAllData(
        dispatcher: CoroutineDispatcher = Dispatchers.IO
    ): Result<Unit> = withContext(dispatcher) {
        val exceptions = mutableListOf<Throwable>()
        countriesRepository.loadCountries()
            .onFailure { exceptions.add(it) }
        proposalsRepository.loadProposalsAndVotes()
            .onFailure { exceptions.add(it) }

        return@withContext exceptions.firstOrNull()?.let { Result.failure(it) }
            ?: Result.success(Unit)
    }
}