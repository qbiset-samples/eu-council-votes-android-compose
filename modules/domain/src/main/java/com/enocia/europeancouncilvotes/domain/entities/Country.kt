package com.enocia.europeancouncilvotes.domain.entities

/**
 * A country participating in the votes.
 */
interface Country {
    val identifier: Int
    val name: String
}