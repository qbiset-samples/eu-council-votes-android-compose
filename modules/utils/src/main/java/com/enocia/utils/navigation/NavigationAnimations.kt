package com.enocia.utils.navigation

import androidx.compose.animation.AnimatedContentTransitionScope
import androidx.compose.animation.EnterTransition
import androidx.compose.animation.ExitTransition
import androidx.compose.animation.core.tween
import androidx.navigation.NavBackStackEntry

object NavigationAnimations {
    private const val TRANSITION_DURATION: Int = 600 // In milliseconds

    fun slideInAnimation(
        scope: AnimatedContentTransitionScope<NavBackStackEntry>,
        initialRoute: Route,
        targetRoute: Route,
        durationMillis: Int = TRANSITION_DURATION
    ): EnterTransition {
        // If initial route is "greater" than target route, navigation is going back and the destination should enter from the left (towards the right).
        val direction = if (initialRoute > targetRoute) {
            AnimatedContentTransitionScope.SlideDirection.Right
        } else {
            AnimatedContentTransitionScope.SlideDirection.Left
        }
        return scope.slideIntoContainer(direction, tween(durationMillis))
    }

    fun slideOutAnimation(
        scope: AnimatedContentTransitionScope<NavBackStackEntry>,
        initialRoute: Route,
        targetRoute: Route,
        durationMillis: Int = TRANSITION_DURATION
    ): ExitTransition {
        // If initial route is "greater" than target route, navigation is going back and the existing screen should exit towards the right.
        val direction = if (initialRoute > targetRoute) {
            AnimatedContentTransitionScope.SlideDirection.Right
        } else {
            AnimatedContentTransitionScope.SlideDirection.Left
        }
        return scope.slideOutOfContainer(direction, tween(durationMillis))
    }
}