@file:JvmName("SafeLog")

package com.enocia.utils

import android.util.Log

/**
 * A wrapper class providing common Log functions that will only be executed if the app is
 * a debug build.
 * @author Quentin BISET (qbiset@gmail.com)
 */
object SafeLog {
    private val isDebugBuild = BuildConfig.DEBUG

    @JvmStatic
    fun d(tag: String?, msg: String) {
        if (isDebugBuild) {
            Log.d(tag, msg)
        }
    }

    @JvmStatic
    fun e(tag: String?, msg: String) {
        if (isDebugBuild) {
            Log.e(tag, msg)
        }
    }

    @JvmStatic
    fun i(tag: String?, msg: String) {
        if (isDebugBuild) {
            Log.i(tag, msg)
        }
    }

    @JvmStatic
    fun v(tag: String?, msg: String) {
        if (isDebugBuild) {
            Log.v(tag, msg)
        }
    }

    @JvmStatic
    fun w(tag: String?, msg: String) {
        if (isDebugBuild) {
            Log.w(tag, msg)
        }
    }

    @JvmStatic
    fun wtf(tag: String?, msg: String) {
        if (isDebugBuild) {
            Log.wtf(tag, msg)
        }
    }

    @JvmStatic
    fun println(priority: Int, tag: String?, msg: String) {
        if (isDebugBuild) {
            Log.println(priority, tag, msg)
        }
    }
}