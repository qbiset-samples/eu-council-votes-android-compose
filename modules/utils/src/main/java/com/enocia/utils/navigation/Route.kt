package com.enocia.utils.navigation

import androidx.navigation.NamedNavArgument

class Route(
    private val level: Int,
    private val baseRoute: String,
    private val mandatoryArguments: List<NamedNavArgument>? = null,
    private val optionalArguments: List<NamedNavArgument>? = null
) : Comparable<Route> {
    /*
    Route should look like:
    "<base_route>/{mandatory1}/{mandatory2}?optional1={optional1}&optional2={optional2}"
    */
    val route: String = buildFullRouteString(
        mandatoryArgsTransform = { "{${it.name}}" },
        optionalArgsTransform = { "${it.name}={${it.name}}" }
    )

    val arguments: List<NamedNavArgument> get() = mandatoryArguments.orEmpty() + optionalArguments.orEmpty()

    /*
    Destination should look like:
    "<base_route>/1/2?optional1=1&optional2=2"
    */
    fun buildDestinationString(
        mandatoryArguments: Map<String, Any>,
        optionalArguments: Map<String, Any>? = null
    ): String {
        this.mandatoryArguments?.forEach {
            require(mandatoryArguments.containsKey(it.name)) { "Argument ${it.name} is missing!" }
        }

        return buildFullRouteString(
            mandatoryArgsTransform = { mandatoryArguments.getValue(it.name).toString() },
            optionalArgsTransform = { navArg ->
                optionalArguments?.get(navArg.name)?.let { "${navArg.name}=$it" }
            }
        )
    }

    private fun buildFullRouteString(
        mandatoryArgsTransform: (NamedNavArgument) -> String,
        optionalArgsTransform: (NamedNavArgument) -> String?
    ): String {
        var route = baseRoute
        buildMandatoryArgsString(mandatoryArgsTransform)?.let { route += it }
        buildOptionalArgsString(optionalArgsTransform)?.let { route += it }
        return route
    }

    /*
    Produced string should look like:
    - "/{arg1}/{arg2}" for route
    - "/1/2" for destination
    */
    private fun buildMandatoryArgsString(
        transform: (NamedNavArgument) -> String
    ): String? = mandatoryArguments?.map { transform(it) }
        ?.reduceOrNull { accumulator, string -> "$accumulator/$string" }
        ?.let { "/$it" }

    /*
    Produced string should look like:
    - "?arg1={arg1}&arg2={arg2}" for route
    - "?arg1=1&arg2=2" for destination
    */
    private fun buildOptionalArgsString(
        transform: (NamedNavArgument) -> String?
    ): String? = optionalArguments?.mapNotNull { transform(it) }
        ?.reduceOrNull { accumulator, string -> "$accumulator&$string" }
        ?.let { "?$it" }

    override fun compareTo(other: Route): Int = level.compareTo(other.level)

    override fun toString(): String = "Route(level: $level, base route: $baseRoute, " +
            "mandatory arguments: ${mandatoryArguments?.map { it.name }}, " +
            "optional arguments: ${optionalArguments?.map { it.name }}, " +
            "full route: $route)"
}