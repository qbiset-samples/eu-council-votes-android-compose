package com.enocia.europeancouncilvotes.api

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.enocia.europeancouncilvotes.api.retrofit.EuCouncilApiFactory
import com.enocia.europeancouncilvotes.api.retrofit.EuCouncilRetrofitDataSource
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class RetrofitIntegrationTest {
    private lateinit var dataSource: EuCouncilRetrofitDataSource

    @Before
    fun createDataSource() {
        dataSource = EuCouncilRetrofitDataSource(EuCouncilApiFactory.build())
    }

    @Test
    @Throws(Exception::class)
    fun getCountries() = runTest {
        val parsedCountries = dataSource.getCountries()
        assert(parsedCountries.isNotEmpty())
    }

    @Test
    @Throws(Exception::class)
    fun getProposals() = runTest {
        val parsedProposals = dataSource.getProposals()
        assert(parsedProposals.isNotEmpty())
    }

    @Test
    @Throws(Exception::class)
    fun getProposalsWithVotes() = runTest {
        val parsedProposalsWithVotes = dataSource.getProposalsWithVotes()
        assert(parsedProposalsWithVotes.isNotEmpty())
    }
}