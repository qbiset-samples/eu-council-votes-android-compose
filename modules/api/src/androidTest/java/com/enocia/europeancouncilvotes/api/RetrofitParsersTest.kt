package com.enocia.europeancouncilvotes.api

import androidx.test.ext.junit.runners.AndroidJUnit4
import com.enocia.europeancouncilvotes.api.retrofit.objects.ProposalAndVotes
import com.enocia.europeancouncilvotes.api.retrofit.objects.RetrofitCountry
import com.enocia.europeancouncilvotes.api.retrofit.objects.RetrofitProposal
import com.enocia.europeancouncilvotes.api.retrofit.objects.RetrofitVote
import com.enocia.europeancouncilvotes.api.retrofit.parsers.CountryParser
import com.enocia.europeancouncilvotes.api.retrofit.parsers.ProposalParser
import com.enocia.europeancouncilvotes.api.retrofit.parsers.VoteParser
import com.enocia.europeancouncilvotes.domain.entities.Vote
import kotlinx.coroutines.test.runTest
import org.json.JSONArray
import org.junit.Assert.assertArrayEquals
import org.junit.Test
import org.junit.runner.RunWith
import java.time.LocalDate

// The JSON library is at least partially dependant on the Android SDK, so these tests need to be instrumented.
@RunWith(AndroidJUnit4::class)
class RetrofitParsersTest {
    @Test
    @Throws(Exception::class)
    fun parseMockCountries() = runTest {
        val parsedCountries = CountryParser.parseCountries(MockCountry.testJson)
        assertArrayEquals(
            MockCountry.buildExpectedCountries().toTypedArray(),
            parsedCountries.toTypedArray()
        )
    }

    @Test
    @Throws(Exception::class)
    fun parseMockProposals() = runTest {
        val parsedProposals = ProposalParser.parseProposals(MockProposal.testJson)
        assertArrayEquals(
            MockProposal.buildExpectedProposals().toTypedArray(),
            parsedProposals.toTypedArray()
        )
    }

    @Test
    @Throws(Exception::class)
    fun parseMockVotes() = runTest {
        val parsedVotes = VoteParser.parseVotes(JSONArray(MockVote.testJson))
        assertArrayEquals(MockVote.buildExpectedVotes().toTypedArray(), parsedVotes.toTypedArray())
    }

    @Test
    @Throws(Exception::class)
    fun parseMockProposalsAndVotes() = runTest {
        val parsedObjects = ProposalParser.parseProposalsAndVotes(MockProposalAndVotes.testJson)
        assertArrayEquals(
            MockProposalAndVotes.buildExpectedProposalAndVotes().toTypedArray(),
            parsedObjects.toTypedArray()
        )
    }
}

private object MockCountry {
    // Keep in mind the API returns a JSON map with irrelevant keys rather than a proper array.
    const val testJson: String =
        "{\"1\":{\"country_id\":\"1\",\"country\":\"Malta\"},\"2\":{\"country_id\":\"2\",\"country\":\"Luxembourg\"}}"

    fun buildExpectedCountries(): List<RetrofitCountry> = listOf(
        RetrofitCountry(1, "Malta"),
        RetrofitCountry(2, "Luxembourg")
    )
}

private object MockProposal {
    const val testJson: String =
        "{\"1691\":{\"vote_id\":\"1691\",\"date\":\"2007-05-31\",\"vote_title\":\"Test Proposal 1\",\"link\":\"http:\\/\\/register.consilium.europa.eu\\/pdf\\/en\\/07\\/st10\\/st10129.en07.pdf\"}, \"1692\":{\"vote_id\":\"1692\",\"date\":\"2008-06-06\",\"vote_title\":\"Test Proposal 2\",\"link\":\"http:\\/\\/register.consilium.europa.eu\\/pdf\\/en\\/08\\/st10\\/st10377.en08.pdf\"}}"

    fun buildProposal1() = RetrofitProposal(
        1691,
        "Test Proposal 1",
        "http://register.consilium.europa.eu/pdf/en/07/st10/st10129.en07.pdf",
        LocalDate.of(2007, 5, 31)
    )

    fun buildProposal2() = RetrofitProposal(
        1692,
        "Test Proposal 2",
        "http://register.consilium.europa.eu/pdf/en/08/st10/st10377.en08.pdf",
        LocalDate.of(2008, 6, 6)
    )

    fun buildExpectedProposals() = listOf(buildProposal1(), buildProposal2())
}

private object MockVote {
    const val testJson: String =
        "[{\"country_id\":\"19\",\"vote_id\":\"1691\",\"vote\":\"For\"},{\"country_id\":\"12\",\"vote_id\":\"1691\",\"vote\":\"Against\"}]"

    fun buildExpectedVotes() = listOf(
        RetrofitVote(1691, 19, Vote.Value.FOR),
        RetrofitVote(1691, 12, Vote.Value.AGAINST)
    )
}

private object MockProposalAndVotes {
    const val testJson: String =
        "{\"1691\":{\"vote_id\":\"1691\",\"date\":\"2007-05-31\",\"vote_title\":\"Test Proposal 1\",\"link\":\"http:\\/\\/register.consilium.europa.eu\\/pdf\\/en\\/07\\/st10\\/st10129.en07.pdf\",\"votes\":${MockVote.testJson}}, \"1692\":{\"vote_id\":\"1692\",\"date\":\"2008-06-06\",\"vote_title\":\"Test Proposal 2\",\"link\":\"http:\\/\\/register.consilium.europa.eu\\/pdf\\/en\\/08\\/st10\\/st10377.en08.pdf\",\"votes\":${MockVote.testJson}}}"

    fun buildProposalAndVotes1() = ProposalAndVotes(
        MockProposal.buildProposal1(),
        MockVote.buildExpectedVotes()
    )

    fun buildProposalAndVotes2() = ProposalAndVotes(
        MockProposal.buildProposal2(),
        MockVote.buildExpectedVotes()
    )

    fun buildExpectedProposalAndVotes() = listOf(
        buildProposalAndVotes1(),
        buildProposalAndVotes2()
    )
}