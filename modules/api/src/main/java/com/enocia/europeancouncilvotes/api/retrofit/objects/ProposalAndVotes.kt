package com.enocia.europeancouncilvotes.api.retrofit.objects

import com.enocia.europeancouncilvotes.domain.entities.Proposal
import com.enocia.europeancouncilvotes.domain.entities.Vote

data class ProposalAndVotes(val proposal: Proposal, val votes: List<Vote>)