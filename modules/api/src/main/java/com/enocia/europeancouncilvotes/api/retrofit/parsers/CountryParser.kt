package com.enocia.europeancouncilvotes.api.retrofit.parsers

import com.enocia.europeancouncilvotes.api.retrofit.objects.RetrofitCountry
import org.json.JSONObject

internal object CountryParser {
    private const val IDENTIFIER_KEY = "country_id"
    private const val NAME_KEY = "country"

    fun parseCountries(jsonString: String): List<RetrofitCountry> {
        try {
            // Keep in mind the API returns a JSON map with irrelevant keys rather than a proper array.
            val rootObject = JSONObject(jsonString)
            val iterator = rootObject.keys()

            val countries = ArrayList<RetrofitCountry>(rootObject.length())

            while (iterator.hasNext()) {
                try {
                    val key = iterator.next()
                    val jsonCountry = rootObject.getJSONObject(key)
                    val country = parseCountry(jsonCountry)
                    countries.add(country)
                } catch (throwable: Throwable) {
                    println(throwable.toString())
                }
            }

            return countries
        } catch (throwable: Throwable) {
            throw Exception("Failed to parse countries from JSON.", throwable)
        }
    }

    fun parseCountry(jsonCountry: JSONObject): RetrofitCountry {
        try {
            val identifier = jsonCountry.getString(IDENTIFIER_KEY)
            val name = jsonCountry.getString(NAME_KEY)
            return RetrofitCountry(identifier, name)
        } catch (throwable: Throwable) {
            throw Exception("Failed to parse country from JSON.", throwable)
        }
    }
}