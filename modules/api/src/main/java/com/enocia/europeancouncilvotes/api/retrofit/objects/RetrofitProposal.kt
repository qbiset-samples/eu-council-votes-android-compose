package com.enocia.europeancouncilvotes.api.retrofit.objects

import com.enocia.europeancouncilvotes.domain.entities.Proposal
import java.time.LocalDate

// The JSON returned by the API sends the identifier as String, remember to convert it to Int.
data class RetrofitProposal(
    override val identifier: Int,
    override val title: String,
    override val pdfLink: String,
    override val date: LocalDate
) : Proposal {
    constructor(
        identifier: String,
        title: String,
        pdfLink: String,
        date: LocalDate
    ) : this(identifier.toInt(), title, pdfLink, date)
}