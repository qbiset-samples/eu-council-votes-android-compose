package com.enocia.europeancouncilvotes.api.retrofit.parsers

import com.enocia.europeancouncilvotes.api.retrofit.objects.ProposalAndVotes
import com.enocia.europeancouncilvotes.api.retrofit.objects.RetrofitProposal
import org.json.JSONObject
import java.time.LocalDate
import java.time.format.DateTimeFormatter

internal object ProposalParser {
    private const val IDENTIFIER_KEY = "vote_id"
    private const val TITLE_KEY = "vote_title"
    private const val PDF_LINK_KEY = "link"
    private const val DATE_KEY = "date"
    private const val VOTES_KEY = "votes"

    fun parseProposals(jsonString: String): List<RetrofitProposal> {
        try {
            // Keep in mind the API returns a JSON map with irrelevant keys rather than a proper array.
            val rootObject = JSONObject(jsonString)
            val rootIterator = rootObject.keys()

            val proposals = ArrayList<RetrofitProposal>(rootObject.length())

            while (rootIterator.hasNext()) {
                try {
                    val key = rootIterator.next()
                    val jsonProposal = rootObject.getJSONObject(key)
                    val proposal: RetrofitProposal = parseProposal(jsonProposal)
                    proposals.add(proposal)
                } catch (throwable: Throwable) {
                    println(throwable.toString())
                }
            }

            return proposals
        } catch (throwable: Throwable) {
            throw Exception("Failed to parse proposals from JSON.", throwable)
        }
    }

    fun parseProposalsAndVotes(jsonString: String): List<ProposalAndVotes> {
        try {
            val rootObject = JSONObject(jsonString)
            val rootIterator = rootObject.keys()

            val proposals = ArrayList<ProposalAndVotes>(rootObject.length())

            while (rootIterator.hasNext()) {
                try {
                    val key = rootIterator.next()
                    val jsonProposal = rootObject.getJSONObject(key)
                    val jsonVotes = jsonProposal.getJSONArray(VOTES_KEY)
                    val proposalAndVotes = ProposalAndVotes(
                        parseProposal(jsonProposal),
                        VoteParser.parseVotes(jsonVotes)
                    )
                    proposals.add(proposalAndVotes)
                } catch (throwable: Throwable) {
                    println(throwable.toString())
                }
            }

            return proposals
        } catch (e: Exception) {
            throw Exception("Failed to parse proposals from JSON.", e)
        }
    }

    fun parseProposal(jsonProposal: JSONObject): RetrofitProposal {
        try {
            val identifier = jsonProposal.getString(IDENTIFIER_KEY)
            val title = jsonProposal.getString(TITLE_KEY).trim().trimIndent()
            val pdfLink = jsonProposal.getString(PDF_LINK_KEY)

            val dateString = jsonProposal.getString(DATE_KEY)
            val date = LocalDate.parse(dateString, DateTimeFormatter.ISO_LOCAL_DATE)

            return RetrofitProposal(identifier, title, pdfLink, date)
        } catch (throwable: Throwable) {
            throw Exception("Failed to parse proposal  from JSON.", throwable)
        }
    }
}