package com.enocia.europeancouncilvotes.api.retrofit.parsers

import com.enocia.europeancouncilvotes.api.retrofit.objects.RetrofitVote
import org.json.JSONArray
import org.json.JSONObject

internal object VoteParser {
    private const val PROPOSAL_IDENTIFIER_KEY = "vote_id"
    private const val COUNTRY_IDENTIFIER_KEY = "country_id"
    private const val VOTE_VALUE_KEY = "vote"

    fun parseVotes(jsonVotes: JSONArray): List<RetrofitVote> {
        try {
            val votes = ArrayList<RetrofitVote>(jsonVotes.length())

            for (index in 0 until jsonVotes.length()) {
                try {
                    // Parsing all needed data from JSON string
                    val jsonVote = jsonVotes.getJSONObject(index)
                    val vote = parseVote(jsonVote)
                    votes.add(vote)
                } catch (throwable: Throwable) {
                    println("$throwable; ${throwable.cause}")
                }
            }

            return votes
        } catch (throwable: Throwable) {
            throw Exception("Failed to parse votes from JSON.", throwable)
        }
    }

    fun parseVote(jsonVote: JSONObject): RetrofitVote {
        try {
            val proposalIdentifier = jsonVote.getString(PROPOSAL_IDENTIFIER_KEY)
            val countryIdentifier = jsonVote.getString(COUNTRY_IDENTIFIER_KEY)
            val voteValue = jsonVote.getString(VOTE_VALUE_KEY)

            return RetrofitVote(proposalIdentifier, countryIdentifier, voteValue)
        } catch (throwable: Throwable) {
            throw Exception("Failed to parse vote from JSON.", throwable)
        }
    }
}