package com.enocia.europeancouncilvotes.api.retrofit.objects

import com.enocia.europeancouncilvotes.domain.entities.Country

// The JSON returned by the API sends the identifier as String, remember to convert it to Int.
data class RetrofitCountry(override val identifier: Int, override val name: String) : Country {
    constructor(identifier: String, name: String) : this(identifier.toInt(), name)
}