package com.enocia.europeancouncilvotes.api.retrofit.objects

import com.enocia.europeancouncilvotes.domain.entities.Vote

// The JSON returned by the API sends identifiers as String, remember to convert them to Int.
data class RetrofitVote(
    override val proposalId: Int,
    override val countryId: Int,
    override val value: Vote.Value
) : Vote {
    constructor(proposalId: String, countryId: String, value: String) : this(
        proposalId.toInt(),
        countryId.toInt(),
        when (value) {
            "For" -> Vote.Value.FOR
            "Against" -> Vote.Value.AGAINST
            "Abstain" -> Vote.Value.ABSTAIN
            "Not participating" -> Vote.Value.NOT_PARTICIPATING
            else -> throw IllegalArgumentException("Vote value \"$value\" is not recognized.")
        }
    )
}