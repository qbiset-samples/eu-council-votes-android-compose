package com.enocia.europeancouncilvotes.api.retrofit

import retrofit2.Retrofit
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET

interface EuCouncilApiService {
    @GET(EuCouncilApiFactory.COUNTRIES_QUERY_PATH)
    suspend fun getCountries(): String

    @GET(EuCouncilApiFactory.PROPOSALS_QUERY_PATH)
    suspend fun getProposals(): String
}

object EuCouncilApiFactory {
    private const val BASE_URL = "http://api.epdb.eu/council/"
    internal const val COUNTRIES_QUERY_PATH = "country"
    internal const val PROPOSALS_QUERY_PATH = "document"

    fun build(): EuCouncilApiService {
        val retrofit = createRetrofit()
        return retrofit.create(EuCouncilApiService::class.java)
    }

    private fun createRetrofit(): Retrofit = Retrofit.Builder()
        .addConverterFactory(ScalarsConverterFactory.create())
        .baseUrl(BASE_URL)
        .build()
}