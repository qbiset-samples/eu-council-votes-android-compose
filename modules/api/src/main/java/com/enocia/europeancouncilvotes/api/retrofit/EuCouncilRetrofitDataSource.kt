package com.enocia.europeancouncilvotes.api.retrofit

import com.enocia.europeancouncilvotes.api.retrofit.objects.ProposalAndVotes
import com.enocia.europeancouncilvotes.api.retrofit.parsers.CountryParser
import com.enocia.europeancouncilvotes.api.retrofit.parsers.ProposalParser
import com.enocia.europeancouncilvotes.domain.entities.Country
import com.enocia.europeancouncilvotes.domain.entities.Proposal

class EuCouncilRetrofitDataSource(private val retrofitService: EuCouncilApiService) {

    // Countries ------
    @Throws(Exception::class)
    suspend fun getCountries(): List<Country> {
        try {
            val countriesJsonString = retrofitService.getCountries()
            return CountryParser.parseCountries(countriesJsonString)
        } catch (e: Exception) {
            throw e
        }
    }

    // Proposals ------
    @Throws(Exception::class)
    suspend fun getProposals(): List<Proposal> {
        try {
            val proposalsJsonString = retrofitService.getProposals()
            return ProposalParser.parseProposals(proposalsJsonString)
        } catch (e: Exception) {
            throw e
        }
    }

    @Throws(Exception::class)
    suspend fun getProposalsWithVotes(): List<ProposalAndVotes> {
        try {
            val proposalsJsonString = retrofitService.getProposals()
            return ProposalParser.parseProposalsAndVotes(proposalsJsonString)
        } catch (e: Exception) {
            throw e
        }
    }
}