package com.enocia.europeancouncilvotes.api.koin

import com.enocia.europeancouncilvotes.api.retrofit.EuCouncilApiFactory
import com.enocia.europeancouncilvotes.api.retrofit.EuCouncilRetrofitDataSource
import org.koin.dsl.module

val KOIN_RETROFIT = module {
    single { EuCouncilApiFactory.build() }
    single { EuCouncilRetrofitDataSource(get()) }
}