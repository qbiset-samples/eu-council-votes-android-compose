## European Council Votes - API

This module contains data sources used to retrieve European Council votes data from a public API.

The only option at the moment is Retrofit, but we could theoretically add other options.