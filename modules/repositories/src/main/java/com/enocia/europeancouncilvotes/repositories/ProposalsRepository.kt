package com.enocia.europeancouncilvotes.repositories

import com.enocia.europeancouncilvotes.api.retrofit.EuCouncilRetrofitDataSource
import com.enocia.europeancouncilvotes.domain.entities.Proposal
import com.enocia.europeancouncilvotes.domain.entities.ProposalAndVotes
import com.enocia.europeancouncilvotes.domain.repositories.ProposalsRepository
import com.enocia.europeancouncilvotes.storage.room.EuCouncilRoomDataSource
import com.enocia.utils.SafeLog
import kotlinx.coroutines.flow.Flow

class ProposalsRepositoryImpl(
    private val retrofitDataSource: EuCouncilRetrofitDataSource,
    private val roomDataSource: EuCouncilRoomDataSource
) : ProposalsRepository {

    companion object {
        private const val TAG = "ProposalsRepositoryImpl"
    }

    override suspend fun loadProposalsAndVotes(): Result<Unit> = runCatching {
        try {
            val proposals = retrofitDataSource.getProposalsWithVotes()
            proposals.forEach {
                roomDataSource.saveProposalAndVotes(it.proposal, it.votes)
            }
        } catch (throwable: Throwable) {
            val exception = Exception("Failed to get proposals and votes from API.", throwable)
            SafeLog.d(TAG, throwable.toString())
            throw exception
        }
    }

    override fun getProposalsAsFlow(): Flow<List<Proposal>> = roomDataSource.getShallowProposalsAsFlow()

    override fun getProposalAndVotes(proposalId: Int): ProposalAndVotes = roomDataSource.getProposalWithVotes(proposalId)
}