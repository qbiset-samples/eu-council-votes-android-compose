package com.enocia.europeancouncilvotes.repositories

import com.enocia.europeancouncilvotes.api.retrofit.EuCouncilRetrofitDataSource
import com.enocia.europeancouncilvotes.domain.repositories.CountriesRepository
import com.enocia.europeancouncilvotes.storage.room.EuCouncilRoomDataSource
import com.enocia.utils.SafeLog

class CountriesRepositoryImpl(
    private val retrofitDataSource: EuCouncilRetrofitDataSource,
    private val roomDataSource: EuCouncilRoomDataSource
) : CountriesRepository {

    companion object {
        private const val TAG = "EuCouncilRepositoryImpl"
    }

    override suspend fun loadCountries(): Result<Unit> = runCatching {
        try {
            val countries = retrofitDataSource.getCountries()
            roomDataSource.saveCountries(countries)
        } catch (throwable: Throwable) {
            val exception = Exception("Failed to get countries from API.", throwable)
            SafeLog.d(TAG, throwable.toString())
            throw exception
        }
    }
}