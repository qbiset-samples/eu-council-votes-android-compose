package com.enocia.europeancouncilvotes.repositories.koin

import com.enocia.europeancouncilvotes.api.koin.KOIN_RETROFIT
import com.enocia.europeancouncilvotes.domain.repositories.CountriesRepository
import com.enocia.europeancouncilvotes.domain.repositories.ProposalsRepository
import com.enocia.europeancouncilvotes.repositories.CountriesRepositoryImpl
import com.enocia.europeancouncilvotes.repositories.ProposalsRepositoryImpl
import com.enocia.europeancouncilvotes.storage.koin.KOIN_ROOM
import org.koin.dsl.module

val KOIN_REPOSITORIES = module {
    includes(KOIN_RETROFIT, KOIN_ROOM)

    single<CountriesRepository> { CountriesRepositoryImpl(get(), get()) }
    single<ProposalsRepository> { ProposalsRepositoryImpl(get(), get()) }
}