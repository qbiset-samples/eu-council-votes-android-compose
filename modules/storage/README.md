## European Council Votes - Storage

This module contains objects required to handle permanent storage of European Council Votes data.

The only option at the moment is Room, but we could theoretically add other storage options.