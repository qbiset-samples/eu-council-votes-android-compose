package com.enocia.europeancouncilvotes.storage

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.enocia.europeancouncilvotes.domain.entities.Vote.Value.*
import com.enocia.europeancouncilvotes.storage.room.dao.CountryDao
import com.enocia.europeancouncilvotes.storage.room.dao.ProposalDao
import com.enocia.europeancouncilvotes.storage.room.dao.VoteDao
import com.enocia.europeancouncilvotes.storage.room.dao.objects.ProposalAndVotes
import com.enocia.europeancouncilvotes.storage.room.database.EuCouncilRoomDatabase
import com.enocia.europeancouncilvotes.storage.room.entities.RoomCountry
import com.enocia.europeancouncilvotes.storage.room.entities.RoomProposal
import com.enocia.europeancouncilvotes.storage.room.entities.RoomVote
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.time.LocalDate

// Testing documentation: http://d.android.com/tools/testing

@RunWith(AndroidJUnit4::class)
class DatabaseTest {
    private lateinit var database: EuCouncilRoomDatabase
    private lateinit var countryDao: CountryDao
    private lateinit var proposalDao: ProposalDao
    private lateinit var voteDao: VoteDao

    @Before
    fun createDatabaseAndDao() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(
            context, EuCouncilRoomDatabase::class.java
        ).build()
        database.run {
            countryDao = getCountryDao()
            proposalDao = getProposalDao()
            voteDao = getVoteDao()
        }
    }

    @After
    @Throws(IOException::class)
    fun closeDatabase() {
        database.close()
    }

    // Insert countries and test retrieved RoomCountry objects.
    @Test
    @Throws(Exception::class)
    fun writeCountriesAndRead() = runTest {
        val originalList = MockCountryFactory.buildCountriesList()

        countryDao.insert(originalList)

        val retrievedList = countryDao.getAllCountries()
        assertArrayEquals(originalList.toTypedArray(), retrievedList.toTypedArray())
    }

    // Insert proposals and test retrieved RoomProposal objects.
    @Test
    @Throws(Exception::class)
    fun writeProposalsAndRead() = runTest {
        val originalList = MockProposalFactory.buildProposalsList()

        proposalDao.insert(originalList)

        val retrievedList = proposalDao.getAllProposals()
        assertArrayEquals(originalList.toTypedArray(), retrievedList.toTypedArray())
    }

    // Insert countries, proposals, votes, and test retrieved ProposalAndVotes objects.
    @Test
    @Throws(Exception::class)
    fun writeVotesAndRead() = runTest {
        // Votes have foreign keys from countries and proposals, so we need to populate those first.
        val countries = MockCountryFactory.buildCountriesList()
        countryDao.insert(countries)

        val proposals = MockProposalFactory.buildProposalsList()
        proposalDao.insert(proposals)

        val votesForProposal1 = MockVoteFactory.buildVotesForProposal(1)
        val votesForProposal2 = MockVoteFactory.buildVotesForProposal(2)
        voteDao.insert(votesForProposal1 + votesForProposal2)

        val fullProposal1: ProposalAndVotes = proposalDao.getProposalWithVotes(1)
        assertEquals(fullProposal1.proposal, proposals[0])
        val retrievedVotes1 = fullProposal1.votes.map { it.vote }
        assertArrayEquals(votesForProposal1.toTypedArray(), retrievedVotes1.toTypedArray())

        val fullProposal2: ProposalAndVotes = proposalDao.getProposalWithVotes(2)
        assertEquals(fullProposal2.proposal, proposals[1])
        val retrievedVotes2 = fullProposal2.votes.map { it.vote }
        assertArrayEquals(votesForProposal2.toTypedArray(), retrievedVotes2.toTypedArray())
    }
}

private object MockCountryFactory {
    fun buildCountry1() = RoomCountry(1, "Country 1")
    fun buildCountry2() = RoomCountry(2, "Country 2")

    fun buildCountriesList() = listOf(buildCountry1(), buildCountry2())
}

private object MockProposalFactory {
    fun buildProposal1() = RoomProposal(1, "Proposal 1", "PDF 1", LocalDate.of(2023, 7, 1))
    fun buildProposal2() = RoomProposal(2, "Proposal 2", "PDF 2", LocalDate.of(2023, 7, 2))

    fun buildProposalsList() = listOf(buildProposal1(), buildProposal2())
}

private object MockVoteFactory {
    fun buildVotesForProposal(proposalId: Int) = listOf(
        RoomVote(proposalId, 1, FOR),
        RoomVote(proposalId, 2, AGAINST)
    )
}