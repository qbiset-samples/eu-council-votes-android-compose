package com.enocia.europeancouncilvotes.storage.room.dao

import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction
import com.enocia.europeancouncilvotes.storage.room.dao.objects.ProposalAndVotes
import com.enocia.europeancouncilvotes.storage.room.entities.RoomProposal
import kotlinx.coroutines.flow.Flow

@Dao
interface ProposalDao : BaseDao<RoomProposal> {
    // Proposals list ------
    @Query(
        "SELECT * FROM ${RoomProposal.TABLE_NAME} " +
                "ORDER BY ${RoomProposal.DATE_COLUMN_NAME}, ${RoomProposal.ID_COLUMN_NAME}"
    )
    fun getAllProposals(): List<RoomProposal>

    @Query(
        "SELECT * FROM ${RoomProposal.TABLE_NAME} " +
                "ORDER BY ${RoomProposal.DATE_COLUMN_NAME}, ${RoomProposal.ID_COLUMN_NAME}"
    )
    fun getAllProposalsAsFlow(): Flow<List<RoomProposal>>

    // Single proposal with associated vote/country tuples ------
    @Transaction
    @Query(
        "SELECT * FROM ${RoomProposal.TABLE_NAME} " +
                "WHERE ${RoomProposal.ID_COLUMN_NAME} = :proposalId " +
                "ORDER BY ${RoomProposal.DATE_COLUMN_NAME}, ${RoomProposal.ID_COLUMN_NAME}"
    )
    fun getProposalWithVotes(proposalId: Int): ProposalAndVotes
}