package com.enocia.europeancouncilvotes.storage.room.dao.objects

import androidx.room.Embedded
import androidx.room.Relation
import com.enocia.europeancouncilvotes.domain.entities.Vote
import com.enocia.europeancouncilvotes.storage.room.entities.RoomCountry
import com.enocia.europeancouncilvotes.storage.room.entities.RoomVote
import com.enocia.europeancouncilvotes.domain.entities.VoteAndCountry as DomainVoteAndCountry

data class VoteAndCountry(
    @Embedded val vote: RoomVote,
    @Relation(
        parentColumn = RoomVote.COUNTRY_ID_COLUMN_NAME,
        entityColumn = RoomCountry.ID_COLUMN_NAME
    )
    override val country: RoomCountry
) : DomainVoteAndCountry {
    override val value: Vote.Value
        get() = vote.value
}