package com.enocia.europeancouncilvotes.storage.room.dao

import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Update

/**
 * A generic type DAO exposing simple insert/update/delete functions.
 */
sealed interface BaseDao<T> {
    // Insert ------
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: T): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entities: List<T>): List<Long>

    // Update ------
    @Update
    fun update(entity: T)

    @Update
    fun update(entities: List<T>)

    // Delete ------
    @Delete
    fun delete(entity: T)

    @Delete
    fun delete(entities: List<T>)
}