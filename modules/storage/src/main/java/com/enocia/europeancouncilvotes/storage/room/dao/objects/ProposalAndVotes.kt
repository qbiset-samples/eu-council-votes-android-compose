package com.enocia.europeancouncilvotes.storage.room.dao.objects

import androidx.room.Embedded
import androidx.room.Relation
import com.enocia.europeancouncilvotes.storage.room.entities.RoomProposal
import com.enocia.europeancouncilvotes.storage.room.entities.RoomVote
import com.enocia.europeancouncilvotes.domain.entities.ProposalAndVotes as DomainProposalAndVotes

data class ProposalAndVotes(
    @Embedded override val proposal: RoomProposal,
    @Relation(
        parentColumn = RoomProposal.ID_COLUMN_NAME,
        entity = RoomVote::class,
        entityColumn = RoomVote.PROPOSAL_ID_COLUMN_NAME
    )
    override val votes: List<VoteAndCountry>
) : DomainProposalAndVotes