package com.enocia.europeancouncilvotes.storage.room

import com.enocia.europeancouncilvotes.domain.entities.Country
import com.enocia.europeancouncilvotes.domain.entities.Proposal
import com.enocia.europeancouncilvotes.domain.entities.Vote
import com.enocia.europeancouncilvotes.storage.room.dao.objects.ProposalAndVotes
import com.enocia.europeancouncilvotes.storage.room.database.EuCouncilRoomDatabase
import com.enocia.europeancouncilvotes.storage.room.entities.RoomCountry
import com.enocia.europeancouncilvotes.storage.room.entities.RoomProposal
import com.enocia.europeancouncilvotes.storage.room.entities.RoomVote
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.distinctUntilChanged

class EuCouncilRoomDataSource(private val database: EuCouncilRoomDatabase) {

    fun saveCountries(countries: List<Country>) {
        val roomCountries = countries.map { RoomCountry(it) }
        database.getCountryDao().insert(roomCountries)
    }

    fun saveProposalAndVotes(proposal: Proposal, votes: List<Vote>) {
        val roomProposal = RoomProposal(proposal)
        database.getProposalDao().insert(roomProposal)
        val roomVotes = votes.map { RoomVote(it) }
        database.getVoteDao().insert(roomVotes)
    }

    fun getShallowProposalsAsFlow(): Flow<List<RoomProposal>> {
     return database.getProposalDao().getAllProposalsAsFlow()
         .distinctUntilChanged()
    }

    fun getProposalWithVotes(proposalId: Int): ProposalAndVotes {
        return database.getProposalDao().getProposalWithVotes(proposalId)
    }
}