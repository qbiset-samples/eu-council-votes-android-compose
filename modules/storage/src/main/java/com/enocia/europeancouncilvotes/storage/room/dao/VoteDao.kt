package com.enocia.europeancouncilvotes.storage.room.dao

import androidx.room.Dao
import com.enocia.europeancouncilvotes.storage.room.entities.RoomVote

@Dao
interface VoteDao : BaseDao<RoomVote>