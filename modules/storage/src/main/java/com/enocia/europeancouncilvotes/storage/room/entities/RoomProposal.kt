package com.enocia.europeancouncilvotes.storage.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.enocia.europeancouncilvotes.domain.entities.Proposal
import java.time.LocalDate
import java.time.format.DateTimeFormatter

@Entity(tableName = RoomProposal.TABLE_NAME)
data class RoomProposal(
    @PrimaryKey
    @ColumnInfo(name = ID_COLUMN_NAME)
    override val identifier: Int,

    @ColumnInfo(name = TITLE_COLUMN_NAME)
    override val title: String,

    @ColumnInfo(name = LINK_COLUMN_NAME)
    override val pdfLink: String,

    @field:TypeConverters(LocalDateConverter::class)
    @ColumnInfo(name = DATE_COLUMN_NAME)
    override val date: LocalDate,
) : Proposal {

    internal companion object Metadata {
        const val TABLE_NAME: String = "proposals"
        const val ID_COLUMN_NAME: String = "identifier"
        const val TITLE_COLUMN_NAME: String = "title"
        const val LINK_COLUMN_NAME: String = "pdf_link"
        const val DATE_COLUMN_NAME: String = "date"
    }

    constructor(proposal: Proposal) : this(
        proposal.identifier,
        proposal.title,
        proposal.pdfLink,
        proposal.date
    )

    // Converters ------
    internal class LocalDateConverter {
        @TypeConverter
        fun dateFromString(string: String?): LocalDate? {
            return string?.let { LocalDate.parse(it, DateTimeFormatter.ISO_LOCAL_DATE) }
        }

        @TypeConverter
        fun dateToString(date: LocalDate?): String? {
            return date?.format(DateTimeFormatter.ISO_LOCAL_DATE)
        }
    }
}