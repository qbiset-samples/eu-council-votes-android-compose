package com.enocia.europeancouncilvotes.storage.room.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.enocia.europeancouncilvotes.storage.room.dao.CountryDao
import com.enocia.europeancouncilvotes.storage.room.dao.ProposalDao
import com.enocia.europeancouncilvotes.storage.room.dao.VoteDao
import com.enocia.europeancouncilvotes.storage.room.entities.RoomCountry
import com.enocia.europeancouncilvotes.storage.room.entities.RoomProposal
import com.enocia.europeancouncilvotes.storage.room.entities.RoomVote

@Database(
    version = 1,
    entities = [RoomProposal::class, RoomCountry::class, RoomVote::class]
)
abstract class EuCouncilRoomDatabase : RoomDatabase() {
    abstract fun getCountryDao(): CountryDao
    abstract fun getProposalDao(): ProposalDao
    abstract fun getVoteDao(): VoteDao

    companion object Factory {
        fun buildDatabase(appContext: Context, name: String): EuCouncilRoomDatabase {
            return Room.databaseBuilder(
                appContext,
                EuCouncilRoomDatabase::class.java,
                name
            ).build()
        }
    }
}