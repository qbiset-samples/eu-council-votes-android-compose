package com.enocia.europeancouncilvotes.storage.koin

import com.enocia.europeancouncilvotes.storage.room.EuCouncilRoomDataSource
import com.enocia.europeancouncilvotes.storage.room.database.EuCouncilRoomDatabase
import org.koin.dsl.module

private const val ROOM_DATABASE_FILENAME: String = "eu-council-votes-database"

val KOIN_ROOM = module {
    // Override this factory in a child module if you want to change the file name.
    single { EuCouncilRoomDatabase.buildDatabase(get(), ROOM_DATABASE_FILENAME) }
    single { EuCouncilRoomDataSource(get()) }
}