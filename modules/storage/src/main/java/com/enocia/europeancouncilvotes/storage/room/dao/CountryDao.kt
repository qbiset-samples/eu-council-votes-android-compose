package com.enocia.europeancouncilvotes.storage.room.dao

import androidx.room.Dao
import androidx.room.Query
import com.enocia.europeancouncilvotes.storage.room.entities.RoomCountry

@Dao
interface CountryDao : BaseDao<RoomCountry> {
    @Query(
        "SELECT * FROM ${RoomCountry.TABLE_NAME} " +
                "ORDER BY ${RoomCountry.NAME_COLUMN_NAME}"
    )
    fun getAllCountries(): List<RoomCountry>
}