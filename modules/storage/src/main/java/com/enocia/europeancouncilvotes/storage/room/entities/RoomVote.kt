package com.enocia.europeancouncilvotes.storage.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.TypeConverter
import androidx.room.TypeConverters
import com.enocia.europeancouncilvotes.domain.entities.Vote

@Entity(
    tableName = RoomVote.TABLE_NAME,
    primaryKeys = [RoomVote.PROPOSAL_ID_COLUMN_NAME, RoomVote.COUNTRY_ID_COLUMN_NAME],
    foreignKeys = [
        ForeignKey(
            entity = RoomProposal::class,
            parentColumns = [RoomProposal.ID_COLUMN_NAME],
            childColumns = [RoomVote.PROPOSAL_ID_COLUMN_NAME],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
        ForeignKey(
            entity = RoomCountry::class,
            parentColumns = [RoomCountry.ID_COLUMN_NAME],
            childColumns = [RoomVote.COUNTRY_ID_COLUMN_NAME],
            onUpdate = ForeignKey.CASCADE,
            onDelete = ForeignKey.CASCADE
        ),
    ],
    indices = [ Index(RoomVote.PROPOSAL_ID_COLUMN_NAME), Index(RoomVote.COUNTRY_ID_COLUMN_NAME) ]
)
data class RoomVote(
    @ColumnInfo(name = PROPOSAL_ID_COLUMN_NAME)
    override val proposalId: Int,

    @ColumnInfo(name = COUNTRY_ID_COLUMN_NAME)
    override val countryId: Int,

    @field:TypeConverters(ValueConverter::class)
    @ColumnInfo(name = VALUE_COLUMN_NAME)
    override val value: Vote.Value
) : Vote {

    internal companion object Metadata {
        const val TABLE_NAME: String = "votes"
        const val PROPOSAL_ID_COLUMN_NAME: String = "proposal_identifier"
        const val COUNTRY_ID_COLUMN_NAME: String = "country_identifier"
        const val VALUE_COLUMN_NAME: String = "value"
    }

    constructor(vote: Vote) : this(vote.proposalId, vote.countryId, vote.value)

    // Converters ------
    internal class ValueConverter {
        @TypeConverter
        fun valueFromOrdinal(ordinal: Int): Vote.Value {
            return ordinal.let { Vote.Value.values()[it] }
        }

        @TypeConverter
        fun valueToOrdinal(value: Vote.Value): Int {
            return value.ordinal
        }
    }
}