package com.enocia.europeancouncilvotes.storage.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.enocia.europeancouncilvotes.domain.entities.Country

@Entity(tableName = RoomCountry.TABLE_NAME)
data class RoomCountry(
    @PrimaryKey
    @ColumnInfo(name = ID_COLUMN_NAME)
    override val identifier: Int,

    @ColumnInfo(name = NAME_COLUMN_NAME)
    override val name: String
) : Country {

    internal companion object Metadata {
        const val TABLE_NAME: String = "countries"
        const val ID_COLUMN_NAME: String = "identifier"
        const val NAME_COLUMN_NAME: String = "name"
    }

    constructor(country: Country) : this(country.identifier, country.name)
}