package com.enocia.europeancouncilvotes.common.extensions

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import kotlinx.coroutines.launch

// Convenience - Toasts ------
fun AppCompatActivity.showToast(stringId: Int, duration: Int = Toast.LENGTH_SHORT) {
    lifecycleScope.launch {
        Toast.makeText(this@showToast, stringId, duration)
            .show()
    }
}

fun AppCompatActivity.showToast(string: String, duration: Int = Toast.LENGTH_SHORT) {
    lifecycleScope.launch {
        Toast.makeText(this@showToast, string, duration)
            .show()
    }
}