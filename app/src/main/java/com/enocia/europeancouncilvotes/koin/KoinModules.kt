package com.enocia.europeancouncilvotes.koin

import androidx.lifecycle.SavedStateHandle
import com.enocia.europeancouncilvotes.activity.details.vm.ProposalDetailsViewModel
import com.enocia.europeancouncilvotes.activity.list.vm.ProposalsListViewModel
import com.enocia.europeancouncilvotes.domain.uc.ProposalDetailsUseCase
import com.enocia.europeancouncilvotes.domain.uc.ProposalsListUseCase
import com.enocia.europeancouncilvotes.repositories.koin.KOIN_REPOSITORIES
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val KOIN_USE_CASES = module {
    includes(KOIN_REPOSITORIES)

    factory { ProposalsListUseCase(get(), get()) }
    factory { ProposalDetailsUseCase(get()) }
}

val KOIN_VIEW_MODELS = module {
    includes(KOIN_USE_CASES)

    viewModel { ProposalsListViewModel(get()) }
    viewModel { (handle: SavedStateHandle) -> ProposalDetailsViewModel(handle, get()) }
}