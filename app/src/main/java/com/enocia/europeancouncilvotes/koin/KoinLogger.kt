package com.enocia.europeancouncilvotes.koin

import com.enocia.utils.SafeLog
import org.koin.core.logger.Level
import org.koin.core.logger.Logger
import org.koin.core.logger.MESSAGE

class KoinLogger : Logger() {

    companion object {
        private const val TAG = "Koin"
    }

    override fun display(level: Level, msg: MESSAGE) {
        when (level) {
            Level.DEBUG -> SafeLog.d(TAG, msg)
            Level.INFO -> SafeLog.i(TAG, msg)
            Level.ERROR -> SafeLog.e(TAG, msg)
            Level.WARNING -> SafeLog.w(TAG, msg)
            Level.NONE -> SafeLog.v(TAG, msg)
        }
    }
}