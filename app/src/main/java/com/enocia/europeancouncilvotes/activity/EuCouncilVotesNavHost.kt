package com.enocia.europeancouncilvotes.activity

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.enocia.europeancouncilvotes.activity.details.ui.ProposalDetailsScreen
import com.enocia.europeancouncilvotes.activity.list.ui.ProposalsListScreen
import com.enocia.europeancouncilvotes.activity.list.ui.ProposalsListScreenParameters
import com.enocia.utils.navigation.NavigationAnimations
import com.enocia.utils.navigation.Route

// NavHost ------
@Composable
fun EuCouncilVotesNavHost(
    navController: NavHostController,
    proposalsListParameters: ProposalsListScreenParameters,
) {
    NavHost(
        navController = navController,
        startDestination = NavigationRoutes.proposalsList.route,
        enterTransition = {
            val initialRoute = NavigationRoutes.getRouteFromString(initialState.destination.route)
            val targetRoute = NavigationRoutes.getRouteFromString(targetState.destination.route)
            NavigationAnimations.slideInAnimation(this, initialRoute, targetRoute)
        },
        exitTransition = {
            val initialRoute = NavigationRoutes.getRouteFromString(initialState.destination.route)
            val targetRoute = NavigationRoutes.getRouteFromString(targetState.destination.route)
            NavigationAnimations.slideOutAnimation(this, initialRoute, targetRoute)
        }
    ) {
        composable(
            route = NavigationRoutes.proposalsList.route
        ) {
            ProposalsListScreen(proposalsListParameters)
        }
        composable(
            route = NavigationRoutes.proposalDetails.route,
            arguments = NavigationRoutes.proposalDetails.arguments
        ) {
            ProposalDetailsScreen(onBackClicked = { navController.navigateUp() })
        }
    }
}

// Extensions ------
fun NavController.navigateToDetailsScreen(proposalId: Int) {
    val mandatoryArgs = NavigationRoutes.buildProposalDetailsArgsMap(proposalId)
    val destination = NavigationRoutes.proposalDetails.buildDestinationString(mandatoryArgs)
    navigate(destination)
}

// Routes ------
object NavigationRoutes {
    const val DETAILS_ARG_PROPOSAL_ID = "proposalId"

    // Proposals List
    val proposalsList = Route(1, "proposals_list")

    // Proposal Details
    val proposalDetails = Route(2, "proposal_details", listOf(
        // String type until Compose can actually use anything other than handcrafted string routes to define destinations.
        navArgument(DETAILS_ARG_PROPOSAL_ID) { NavType.StringType }
    ))

    fun buildProposalDetailsArgsMap(proposalId: Int) = mapOf(DETAILS_ARG_PROPOSAL_ID to proposalId)

    // Common
    // TODO: check in which situations the route could be null to handle gracefully.
    fun getRouteFromString(route: String?): Route = when (route) {
        proposalsList.route -> proposalsList
        proposalDetails.route -> proposalDetails
        else -> throw NoSuchElementException("No route exists for $route")
    }
}