package com.enocia.europeancouncilvotes.activity.list.ui

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.enocia.europeancouncilvotes.R
import com.enocia.europeancouncilvotes.common.extensions.materialTopAppBarColors

@OptIn(ExperimentalMaterial3Api::class)
@Preview(showBackground = true)
@Composable
fun ProposalsListTopBar(onRefreshClicked: () -> Unit = {}) {
    TopAppBar(
        title = { Text(stringResource(R.string.app_name)) },
        actions = {
            IconButton(onClick = onRefreshClicked) {
                Icon(
                    Icons.Default.Refresh,
                    stringResource(R.string.proposals_list_refresh_button_description)
                )
            }
        },
        colors = TopAppBarDefaults.materialTopAppBarColors()
    )
}