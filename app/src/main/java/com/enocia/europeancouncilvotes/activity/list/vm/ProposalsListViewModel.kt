package com.enocia.europeancouncilvotes.activity.list.vm

import android.content.SharedPreferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.enocia.europeancouncilvotes.domain.entities.Proposal
import com.enocia.europeancouncilvotes.domain.uc.ProposalsListUseCase
import com.enocia.utils.SafeLog
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.mapLatest
import kotlinx.coroutines.flow.stateIn
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class ProposalsListViewModel(private val useCase: ProposalsListUseCase) : ViewModel() {

    companion object {
        private const val TAG = "EuCouncilVotesViewModel"
        private const val DEFAULT_SUBSCRIPTION_TIMEOUT: Long = 5000 // In milliseconds.

        private const val PREFERENCES_LAST_REQUEST_KEY = "LastRequestDate"
    }

    private val sharingStrategy: SharingStarted
        get() = SharingStarted.WhileSubscribed(DEFAULT_SUBSCRIPTION_TIMEOUT)

    private val preferencesDateFormatter = DateTimeFormatter.ISO_LOCAL_DATE
    private var apiQueryJob: Deferred<Result<Unit>>? = null

    @OptIn(ExperimentalCoroutinesApi::class)
    val proposalsListFlow: StateFlow<List<ItemViewData>> = useCase.getProposalsAsFlow()
        .mapLatest {
            val dateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)
            return@mapLatest it.mapIndexed { index, proposal ->
                ItemViewData(index, proposal, dateFormatter)
            }
        }.stateIn(viewModelScope, sharingStrategy, listOf())

    private val _isQueryingApiFlow = MutableStateFlow(false)
    val isQueryingApiFlow: StateFlow<Boolean>
        get() = _isQueryingApiFlow.asStateFlow()

    // Fetch ------
    /**
     * Tries to fetch data from the web API and store it locally, either at activity creation if
     * the last successful fetch did not occur today or when the user taps the refresh button.
     *
     * Returns a [Result] object so we can display a Toast if an error occurred.
     *
     * If this function is called while the previous fetch attempt is still running, the previous
     * attempt will be cancelled.
     */
    fun fetchDataFromApiAsync(): Deferred<Result<Unit>> {
        apiQueryJob?.cancel()
        val asyncJob = viewModelScope.async {
            _isQueryingApiFlow.value = true
            val result = useCase.loadAllData()
            _isQueryingApiFlow.value = false
            return@async result
        }
        apiQueryJob = asyncJob
        return asyncJob
    }

    // Shared Preferences ------
    /**
     * Checks if the last successful fetch was made today or not.
     */
    fun shouldAutomaticallyFetchDataFromApi(
        sharedPreferences: SharedPreferences
    ): Boolean = sharedPreferences.getString(PREFERENCES_LAST_REQUEST_KEY, null)?.let {
        val lastRequestDate = LocalDate.parse(it, preferencesDateFormatter)
        val currentDate = LocalDate.now()
        return@let currentDate.isAfter(lastRequestDate)
    } ?: true

    fun saveSuccessfulFetchDate(sharedPreferences: SharedPreferences) {
        try {
            val editor = sharedPreferences.edit()
            editor.putString(
                PREFERENCES_LAST_REQUEST_KEY,
                preferencesDateFormatter.format(LocalDate.now())
            )
            editor.apply()
        } catch (throwable: Throwable) {
            val message = "Exception when trying to save successful fetch date: $throwable"
            SafeLog.e(TAG, message)
        }
    }

    // Nested classes ------
    /* Compose might not add item views in order, which causes problems if we only use a proposal's
    identifier as key for the lazy column's state: the column will keep following the first item
    rendered until the column is fully populated, which might result in the column being automatically
    scrolled anywhere but the top. To keep this from happening we need to take into account the
    proposal's index in the underlying list: the resulting hashcode should both keep the column at the
    top during the initial rendering and keep the scroll's position during configuration change. */
    data class ItemViewData(
        val index: Int,
        val identifier: Int,
        val title: String,
        val formattedDate: String
    ) {
        constructor(index: Int, proposal: Proposal, dateFormatter: DateTimeFormatter) : this(
            index,
            proposal.identifier,
            proposal.title,
            dateFormatter.format(proposal.date)
        )
    }
}