package com.enocia.europeancouncilvotes.activity.list.ui

import android.content.SharedPreferences
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.enocia.europeancouncilvotes.activity.list.vm.ProposalsListViewModel
import kotlinx.coroutines.launch
import org.koin.androidx.compose.koinViewModel

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun ProposalsListScreen(
    parameters: ProposalsListScreenParameters,
    viewModel: ProposalsListViewModel = koinViewModel(),
) {
    /* Initial fetch - will check shared preferences to see if data has been fetched recently,
    in which case we skip the API call. */
    LaunchedEffect(true) {
        fetchProposalsListData(
            viewModel,
            parameters.sharedPreferences,
            parameters.onFetchError,
            false
        )
    }

    val coroutineScope = rememberCoroutineScope()
    Surface {
        Scaffold(
            topBar = {
                ProposalsListTopBar(onRefreshClicked = {
                    coroutineScope.launch {
                        fetchProposalsListData(
                            viewModel,
                            parameters.sharedPreferences,
                            parameters.onFetchError,
                            true
                        )
                    }
                })
            },
            content = {
                val proposals = viewModel.proposalsListFlow.collectAsState()
                val isQueryingApi = viewModel.isQueryingApiFlow.collectAsState()

                Box(
                    modifier = Modifier
                        .fillMaxSize()
                        .padding(it)
                        .consumeWindowInsets(it),
                    contentAlignment = Alignment.Center
                ) {
                    ProposalsLazyList(proposals.value, parameters.onProposalClicked)
                    if (isQueryingApi.value) {
                        CircularProgressIndicator()
                    }
                }
            }
        )
    }
}

class ProposalsListScreenParameters(
    val sharedPreferences: SharedPreferences,
    val onFetchError: (Throwable) -> Unit,
    val onProposalClicked: (proposalId: Int) -> Unit,
)

private suspend fun fetchProposalsListData(
    viewModel: ProposalsListViewModel,
    sharedPreferences: SharedPreferences,
    onFetchError: (Throwable) -> Unit,
    forceFetch: Boolean,
) {
    if (forceFetch || viewModel.shouldAutomaticallyFetchDataFromApi(sharedPreferences)) {
        viewModel.fetchDataFromApiAsync().await()
            .onSuccess { viewModel.saveSuccessfulFetchDate(sharedPreferences) }
            .onFailure { onFetchError(it) }
    }
}

// TODO: mock view model and create preview