package com.enocia.europeancouncilvotes.activity.details.ui

import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.enocia.europeancouncilvotes.R
import com.enocia.europeancouncilvotes.common.extensions.materialTopAppBarColors

@OptIn(ExperimentalMaterial3Api::class)
@Preview(showBackground = true)
@Composable
fun ProposalDetailsTopBar(onBackClicked: () -> Unit = {}) {
    TopAppBar(
        title = { Text(stringResource(R.string.app_name)) },
        navigationIcon = {
            IconButton(onClick = onBackClicked) {
                Icon(
                    Icons.Default.ArrowBack,
                    stringResource(R.string.proposal_details_back_button_description)
                )
            }
        },
        colors = TopAppBarDefaults.materialTopAppBarColors()
    )
}