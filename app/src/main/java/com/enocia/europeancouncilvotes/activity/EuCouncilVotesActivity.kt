package com.enocia.europeancouncilvotes.activity

import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.enocia.europeancouncilvotes.R
import com.enocia.europeancouncilvotes.activity.list.ui.ProposalsListScreenParameters
import com.enocia.europeancouncilvotes.common.extensions.showToast

class EuCouncilVotesActivity : AppCompatActivity() {

    companion object {
        private const val TAG = "EuCouncilVotesActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            val navController: NavHostController = rememberNavController()
            val proposalsListParameters = buildProposalsListParameters(navController)
            EuCouncilVotesNavHost(navController, proposalsListParameters)
        }
    }

    private fun buildProposalsListParameters(navController: NavHostController) = ProposalsListScreenParameters(
        sharedPreferences = getPreferences(MODE_PRIVATE),
        onFetchError = {
            val message = getString(
                R.string.proposals_list_refresh_failure,
                it.cause?.localizedMessage
            )
            showToast(message, Toast.LENGTH_LONG)
        },
        onProposalClicked = { proposalId ->
            navController.navigateToDetailsScreen(proposalId)
        }
    )
}