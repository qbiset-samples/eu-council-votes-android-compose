package com.enocia.europeancouncilvotes.activity.details.vm

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.enocia.europeancouncilvotes.R
import com.enocia.europeancouncilvotes.activity.NavigationRoutes
import com.enocia.europeancouncilvotes.domain.entities.Vote
import com.enocia.europeancouncilvotes.domain.uc.ProposalDetailsUseCase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import com.enocia.europeancouncilvotes.domain.entities.ProposalAndVotes as DomainProposalAndVotes
import com.enocia.europeancouncilvotes.domain.entities.VoteAndCountry as DomainVoteAndCountry

class ProposalDetailsViewModel(
    savedStateHandle: SavedStateHandle,
    private val useCase: ProposalDetailsUseCase
) : ViewModel() {

    companion object {
        private const val TAG = "ProposalDetailsViewModel"
    }

    private val _proposalFlow = MutableStateFlow<ProposalAndVotes?>(null)
    val proposalFlow: StateFlow<ProposalAndVotes?>
        get() = _proposalFlow.asStateFlow()

    init {
        val navArg = savedStateHandle.get(NavigationRoutes.DETAILS_ARG_PROPOSAL_ID) as? String
        val proposalId: Int = checkNotNull(navArg?.toInt()) {
            "Expected navigation argument ${NavigationRoutes.DETAILS_ARG_PROPOSAL_ID} not found in saved state handle."
        }

        loadProposalAndVotes(proposalId)
    }

    // Flow initialisation ------
    private fun loadProposalAndVotes(
        proposalId: Int,
        dispatcher: CoroutineDispatcher = Dispatchers.IO
    ) = viewModelScope.launch(dispatcher) {
        val dateFormatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG)
        val proposalAndVotes = ProposalAndVotes(
            useCase.getProposalDetails(proposalId),
            dateFormatter
        )
        _proposalFlow.value = proposalAndVotes
    }

    // Nested classes ------
    class ProposalAndVotes(
        val proposalTitle: String,
        val proposalFormattedDate: String,
        val votes: List<VoteAndCountry>
    ) {
        constructor(
            proposalAndVotes: DomainProposalAndVotes,
            dateFormatter: DateTimeFormatter
        ) : this(
            proposalAndVotes.proposal.title,
            dateFormatter.format(proposalAndVotes.proposal.date),
            proposalAndVotes.votes.map { VoteAndCountry(it) }.sortedBy { it.countryName }
        )
    }

    class VoteAndCountry(
        val countryName: String,
        val voteStringId: Int)
    {
        constructor(voteAndCountry: DomainVoteAndCountry) : this(
            voteAndCountry.country.name,
            when (voteAndCountry.value) {
                Vote.Value.FOR -> R.string.vote_for
                Vote.Value.AGAINST -> R.string.vote_against
                Vote.Value.ABSTAIN -> R.string.vote_abstain
                Vote.Value.NOT_PARTICIPATING -> R.string.vote_not_participant
            }
        )
    }
}