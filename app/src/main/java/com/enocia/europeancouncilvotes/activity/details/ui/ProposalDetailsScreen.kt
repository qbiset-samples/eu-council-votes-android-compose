package com.enocia.europeancouncilvotes.activity.details.ui

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.consumeWindowInsets
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import com.enocia.europeancouncilvotes.activity.details.vm.ProposalDetailsViewModel
import org.koin.androidx.compose.koinViewModel

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun ProposalDetailsScreen(
    onBackClicked: () -> Unit,
    viewModel: ProposalDetailsViewModel = koinViewModel()
) {
    val vmProposal = viewModel.proposalFlow.collectAsState()

    Surface {
        Scaffold(
            topBar = {
                ProposalDetailsTopBar(onBackClicked = onBackClicked)
            },
            content = {
                val proposalAndVotes = vmProposal.value
                val modifier = Modifier
                    .fillMaxSize()
                    .padding(it)
                    .consumeWindowInsets(it)
                if (proposalAndVotes != null) {
                    ProposalDetailsContent(proposalAndVotes, modifier)
                } else {
                    Box(contentAlignment = Alignment.Center, modifier = modifier) {
                        CircularProgressIndicator()
                    }
                }
            }
        )
    }
}

// TODO: mock view model and create preview