package com.enocia.europeancouncilvotes.activity.list.ui

import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.ui.tooling.preview.Preview
import com.enocia.europeancouncilvotes.activity.list.vm.ProposalsListViewModel

@Composable
fun ProposalsLazyList(
    proposals: List<ProposalsListViewModel.ItemViewData>,
    onItemClicked: (proposalId: Int) -> Unit
) {
    val listState = rememberLazyListState()
    LazyColumn(state = listState) {
        items(proposals, key = { it.hashCode() }) {
            ProposalsListItem(it, onItemClicked)
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun ProposalsLazyListPreview() {
    val mockViewData = listOf(
        ProposalsListViewModel.ItemViewData(1, 1, "Proposal 1", "3 August 2023"),
        ProposalsListViewModel.ItemViewData(2, 2, "Proposal 2", "4 August 2023"),
        ProposalsListViewModel.ItemViewData(3, 3, "Proposal 3", "5 August 2023"),
    )
    ProposalsLazyList(mockViewData) {}
}