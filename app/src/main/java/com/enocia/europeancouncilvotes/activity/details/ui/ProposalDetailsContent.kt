package com.enocia.europeancouncilvotes.activity.details.ui

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.dimensionResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.enocia.europeancouncilvotes.R
import com.enocia.europeancouncilvotes.activity.details.vm.ProposalDetailsViewModel
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

@Composable
fun ProposalDetailsContent(
    proposalAndVotes: ProposalDetailsViewModel.ProposalAndVotes,
    modifier: Modifier
) {
    val listState = rememberLazyListState()
    LazyColumn(
        state = listState,
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
    ) {
        item {
            ProposalDetailsSection(
                proposalAndVotes.proposalTitle,
                proposalAndVotes.proposalFormattedDate
            )
            Spacer(Modifier.height(dimensionResource(R.dimen.proposal_details_vertical_padding)))
            ProposalDetailsVotesHeader()
        }
        items(proposalAndVotes.votes) {
            ProposalDetailsVote(it)
        }
    }
}

@Preview(showSystemUi = true)
@Composable
private fun ProposalDetailsContentPreview() {
    val mockVotes = listOf(
        ProposalDetailsViewModel.VoteAndCountry("France", R.string.vote_for),
        ProposalDetailsViewModel.VoteAndCountry("Belgium", R.string.vote_abstain),
        ProposalDetailsViewModel.VoteAndCountry("Germany", R.string.vote_against)
    )
    val mockProposalAndVotes = ProposalDetailsViewModel.ProposalAndVotes(
        "Preview Proposal",
        DateTimeFormatter.ofLocalizedDate(FormatStyle.LONG).format(LocalDate.now()),
        mockVotes
    )
    ProposalDetailsContent(mockProposalAndVotes, modifier = Modifier.fillMaxSize())
}

@Composable
private fun ProposalDetailsSection(title: String, formattedDate: String) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                dimensionResource(R.dimen.proposal_details_horizontal_padding),
                dimensionResource(R.dimen.proposal_details_vertical_padding)
            )
    ) {
        Text(
            formattedDate,
            style = MaterialTheme.typography.titleSmall,
            fontSize = 16.sp,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
        )
        Spacer(Modifier.height(8.dp))
        Text(
            title,
            style = MaterialTheme.typography.bodyLarge,
            fontSize = 20.sp,
            textAlign = TextAlign.Center
        )
    }
}

@Composable
private fun ProposalDetailsVotesHeader() {
    Text(
        stringResource(R.string.proposal_details_votes_header),
        style = MaterialTheme.typography.titleSmall,
        fontSize = 16.sp,
        fontWeight = FontWeight.Bold,
    )
}

@Composable
private fun ProposalDetailsVote(vote: ProposalDetailsViewModel.VoteAndCountry) {
    Row(
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .fillMaxWidth()
            .padding(
                dimensionResource(R.dimen.proposal_details_horizontal_padding),
                dimensionResource(R.dimen.vote_vertical_padding)
            )
    ) {
        Text(
            vote.countryName,
            style = MaterialTheme.typography.bodyLarge,
            fontSize = 18.sp
        )
        Text(
            stringResource(vote.voteStringId),
            style = MaterialTheme.typography.bodyLarge,
            fontSize = 18.sp
        )
    }
}