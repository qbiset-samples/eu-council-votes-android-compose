package com.enocia.europeancouncilvotes.activity.list.ui

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.KeyboardArrowRight
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.sp
import com.enocia.europeancouncilvotes.R
import com.enocia.europeancouncilvotes.activity.list.vm.ProposalsListViewModel

@Composable
fun ProposalsListItem(
    viewData: ProposalsListViewModel.ItemViewData,
    onClick: (proposalId: Int) -> Unit
) {
    Column(
        modifier = Modifier.clickable(
            onClickLabel = stringResource(R.string.proposals_list_item_click_accessibility)
        ) { onClick(viewData.identifier) }
    ) {
        ListItem(
            overlineContent = {
                Text(
                    viewData.formattedDate,
                    style = MaterialTheme.typography.titleSmall,
                    //fontSize = 15.sp,
                    fontWeight = FontWeight.Bold
                )
            },
            headlineContent = {
                Text(
                    viewData.title,
                    style = MaterialTheme.typography.bodyLarge,
                    fontSize = 18.sp,
                    maxLines = 3,
                    overflow = TextOverflow.Ellipsis
                )
            },
            trailingContent = {
                Icon(Icons.Default.KeyboardArrowRight, null)
            },
        )
        Divider(thickness = Dp.Hairline)
    }
}

@Preview(name = "Item view", widthDp = 320, showBackground = true)
@Composable
private fun ProposalsListItemPreview() {
    val mockViewData =
        ProposalsListViewModel.ItemViewData(1, 1, "Preview Proposal", "3 August 2023")
    ProposalsListItem(mockViewData) {}
}