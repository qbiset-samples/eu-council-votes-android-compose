package com.enocia.europeancouncilvotes

import android.app.Application
import com.enocia.europeancouncilvotes.koin.KOIN_VIEW_MODELS
import com.enocia.europeancouncilvotes.koin.KoinLogger
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class EuCouncilVotesApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            KoinLogger()
            androidContext(this@EuCouncilVotesApplication)
            modules(KOIN_VIEW_MODELS)
        }
    }
}